LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY decoder IS
	PORT(clk   :in std_logic;
		  data  :in std_logic_vector(3 DOWNTO 0);
		  ready :in std_logic;
		  cifra :out std_logic_vector(6 DOWNTO 0));
END;


ARCHITECTURE arch_decoder OF decoder IS

BEGIN
	
	PROCESS (ready,clk)
		BEGIN
		IF rising_edge(clk) THEN 
			IF ready = '1' THEN
			
				CASE data IS
					WHEN "0000" => cifra <= "1000000"; --0 
					WHEN "0001" => cifra <= "1111001"; --1
					WHEN "0010" => cifra <= "0100100"; --2
					WHEN "0011" => cifra <= "0110000"; --3 
					WHEN "0100" => cifra <= "0011001"; --4 
					WHEN "0101" => cifra <= "0010010"; --5 
					WHEN "0110" => cifra <= "0000010"; --6 
					WHEN "0111" => cifra <= "1111000"; --7 
					WHEN "1000" => cifra <= "0000000"; --8 
					WHEN "1001" => cifra <= "0010000"; --9 
					WHEN "1010" => cifra <= "0001000"; --A 
					WHEN "1011" => cifra <= "0000011"; --b 
					WHEN "1100" => cifra <= "1000110"; --C 
					WHEN "1101" => cifra <= "0100001"; --d 
					WHEN "1110" => cifra <= "0000110"; --E 
					WHEN "1111" => cifra <= "0001110"; --F
					WHEN OTHERS => cifra <= "0111111"; --ACCENDO SOLO IL LED CENTRALE PER ERRORE
					END CASE;
				
				END IF;
			END IF;
		
		
		END PROCESS;



END;