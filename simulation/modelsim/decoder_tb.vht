-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/16/2018 10:06:59"
                                                            
-- Vhdl Test Bench template for design  :  decoder
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY decoder_tb_vhd_tst IS
END decoder_tb_vhd_tst;
ARCHITECTURE decoder_arch OF decoder_tb_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL cifra : STD_LOGIC_VECTOR(6 DOWNTO 0) := "0000000";
SIGNAL data : STD_LOGIC_VECTOR(3 DOWNTO 0):= "0000";
SIGNAL ready : STD_LOGIC := '1';
SIGNAL clk : STD_LOGIC := '0';
SIGNAL period :time := 20 ns;
COMPONENT decoder
	PORT (
	cifra : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	clk  : IN STD_LOGIC ;
	data : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
	ready : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : decoder
	PORT MAP (
-- list connections between master ports and signals
	cifra => cifra,
	clk => clk,
	data => data,
	ready => ready
	);
clock : PROCESS                                                                                   
BEGIN

  clk <= '0';
  WAIT FOR period/2;
  clk <= '1';
  WAIT FOR period/2;
                                                                             
END PROCESS clock;                                           
always : PROCESS                                                                                   
BEGIN
                                                         
   ready <= '0', '1' AFTER 2*period, '0' AFTER 3*period, '1' AFTER 7*period;
   data <= "0000", "0110" AFTER period, "0011" AFTER 4*period, "0101" AFTER 7*period;	
   WAIT FOR 10*period;
                                                      
END PROCESS always;                                          
END decoder_arch;
