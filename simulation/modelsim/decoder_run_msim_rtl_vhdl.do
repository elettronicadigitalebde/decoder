transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {D:/UNIVERSITA/magistrale/2a1s/ElettronicaDigitale/progetto/decoder/decoder.vhd}

vcom -93 -work work {D:/UNIVERSITA/magistrale/2a1s/ElettronicaDigitale/progetto/decoder/simulation/modelsim/decoder_tb.vht}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneii -L rtl_work -L work -voptargs="+acc"  decoder_tb

add wave *
view structure
view signals
run 80 ns
